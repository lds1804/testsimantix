FROM python:3.5

EXPOSE 4000

RUN mkdir /code

WORKDIR /code

RUN mkdir /temporario

RUN export TMPDIR=/temporario

ADD requirements.txt /code/

RUN pip install -r requirements.txt

ADD . /code/

CMD python app.py
